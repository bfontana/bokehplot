# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    version='0.1.0',
    url='https://bitbucket.org/bfontana/bokehplot',
    packages=find_packages(exclude=('tests', 'docs')),
)
