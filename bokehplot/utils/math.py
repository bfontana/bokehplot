from abc import ABCMeta, abstractmethod
import numpy as np

class MathFunctions(object):
    __metaclass__ = ABCMeta
    def __init__(self):
        self.bounds_ = None

    @property
    def bounds(self):
        return self.bounds_
    @bounds.setter
    def bounds(self, value):
        self.bounds_ = value
    @bounds.deleter
    def bounds(self):
        del self.bounds_

    @staticmethod
    def create(func_string):
        if func_string == 'gaus':
            func_obj = Gaussian()
        elif func_string == 'linear':
            func_obj = Linear()
        else:
            raise ValueError('BokehPlot.MathFunctions.create(): Function {} is not supported.'.format(func_string))
        return func_obj

    @abstractmethod
    def func(self):
        raise NotImplementedError('BokehPlot.MathFunctions.func()')

class Gaussian(MathFunctions):
    def __init__(self):
        super(Gaussian, self).__init__()
        self.bounds_ = ((0,-np.inf,0), (np.inf,np.inf,np.inf))

    @staticmethod
    def func(x, *p):
        A, mu, sigma = p
        return A*np.exp(-(x-mu)**2/(2.*sigma**2))

class Linear(MathFunctions):
    def __init__(self):
        super(Linear, self).__init__()
        self.bounds_ = ((-np.inf, -np.inf), (np.inf, np.inf))

    @staticmethod
    def func(x, *p):
        m, b = p
        return x*m + b
