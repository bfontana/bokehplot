"""
Provides data analysis utilities.
"""
import numpy as np
import pandas as pd

def has_many_elements(data):
    """Verifies if 'data' has more than one element"""
    if isinstance(data, pd.Series):
        ret = False
    elif isinstance(data, (tuple, list)):
        ret = len(data[0]) > 1
    else:
        ret = False
    return ret

def convert_list_to_numpy(data):
    """
    Converts 'data' to a Numpy array if required.
    """
    if isinstance(data, (tuple,list)):
        return np.array(data)
    return data
