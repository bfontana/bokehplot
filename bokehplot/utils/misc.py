"""
Provides miscellaneous utilities.
"""
import os
import numpy as np
from bokeh.models import ColorBar, LinearColorMapper, LogColorMapper, BasicTicker, LogTicker
from bokeh.models import Axis, Title, Legend, Toolbar, Line
import bokeh.palettes as palettes
import colorcet

def basename(string):
    """Shorthand for basename utility"""
    return os.path.basename(string)

def error_handler(string, caller, message):
    """Handles errors"""
    full_string = '[' + caller + '] ' + message
    if string == 'value':
        return ValueError(full_string)
    if string == 'type':
        return TypeError(full_string)
    if string == 'runtime':
        return RuntimeError(full_string)
    return ValueError('[utils] error_handler got an unexpected type.')

def kw_lists(s):
    if s == 'axis':
        return ( [ 'x.' + x for x in dir(Axis) if '__' not in x ] +
                 [ 'y.' + x for x in dir(Axis) if '__' not in x ] )
    if s == 'title':
        return [ 't.' + x for x in dir(Title) if '__' not in x ]
    if s == 'legend':
        return [ 'l.' + x for x in dir(Legend) if '__' not in x ]
    if s=='line':
        return [ x for x in dir(Line) if not x.startswith('_') and not x.startswith('__') ]
    if s == 'toolbar':
        return [ 'toolbar.' + x for x in dir(Toolbar) if '__' not in x ]
    if s == 'source_params':
        return ['color', 'size']
    error_handler('value', 'List ' + s + ' is not defined.')
        
def find_nearest_color(array, value, palette):
    """Given an array and a color palette, it returns the color corresponding to the closer value in 'array' to 'value'"""
    _centres = (array[:-1] + array[1:])/2
    _idx = (np.abs(_centres - value)).argmin()
    return palette[_idx]

def draw_color_bar(figure, palette, min, max, scale='linear'):
    """Draws a color bar, useful for 2d graphs or histograms, based on a colour palette and ranges."""
    if scale == 'linear':
        color_mapper = LinearColorMapper(palette=palette, low=min, high=max)
        ticker = BasicTicker()
    elif scale == 'log':
        if min<0:
            raise ValueError('BokehPlot.draw_color_bar(): cannot use log scale for negative counts.')
        color_mapper = LogColorMapper(palette=palette, low=0.001, high=max)
        ticker = LogTicker()
    else:
        raise ValueError("BokehPlot.utils.draw_color_bar: scale={} not supported. Use 'linear' or 'log'.".format(scale))
    color_bar = ColorBar(color_mapper=color_mapper, ticker=ticker,
                         label_standoff=6, border_line_color=None, location=(0,0))
    figure.add_layout(color_bar, 'right')

def select_style(style):
    """Unpacks the provided style into a texture, its weight, and a color palette"""
    if style is None or len(style.split('%')) == 1: #type or type+palette
        texture, weight, palette = None, None, 'Inferno'
    elif len(style.split('%')) == 2: #type or type+palette
        style, palette = style.split('%')
        texture, weight = None, 0
    elif len(style.split('%')) == 3:
        raise ValueError('BokehPlot.Histogram2D: did you forget to specify the palette?')
    elif len(style.split('%')) == 4:
        s_ = style.split('%')
        print(s_)
        style, texture, weight, palette = s_
    else:
        raise ValueError('BokehPlot.Histogram2D: style badly specified.')

    invert = False
    if palette[:4] == 'inv_':
        invert = True
        palette = palette[4:]
        
    if palette in ('Greys', 'Inferno', 'Magma', 'Plasma', 'Viridis', 'Cividis', 'Turbo'):
        palette = getattr(palettes, palette)
        palette = palette[256]
    elif palette in colorcet.__dict__.keys():
        palette = getattr(colorcet, palette)
    else:
        raise ValueError('You picked the `{}` palette. Choose instead one of the following supported color palettes: Greys, Inferno, Magma, Plasma, Viridis, Cividis, Turbo directly form Bokeh, or else one of the many available from the colorcet package: https://colorcet.holoviz.org/user_guide/index.html.'.format(palette))

    if invert:
        palette = palette[::-1]
    return style, texture, weight, palette
