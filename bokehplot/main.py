__all__ = ['BokehPlot']

import os
import warnings
import numpy as np

from bokeh.layouts import gridplot, layout as bokehlayout
from bokeh.io import save, export_png, show as bokehshow
from bokeh.io.output import output_file

from bokehplot.structure.stack import Stack

class BokehPlot():
    def __init__(self, filenames = "plot.html", nframes=1, nfigs=1, nwidgets=0,
                 fig_width=450, fig_height=300):
        if isinstance(filenames, (tuple, list)):
            assert(len(filenames) == nframes)
            for name in filenames:
                self.check_html(name)
            self._filenames = filenames
        else:
            self.check_html(filenames)
            self._filenames = [filenames] * nframes
        self.fig_height = fig_height
        self.fig_width = fig_width
        self._stack = Stack(nframes, nfigs, nwidgets, fig_width, fig_height)
        self._current_fig = {'frame': 0, 'figure': -1}
        self._current_widg = {'frame': 0, 'widget': -1}
        
    @property
    def filename(self):
        return self._filename
    @filename.setter
    def filename(self, value):
        self._filename = value
    @filename.deleter
    def filename(self):
        del self._filename

    @property
    def current_fig(self):
        return self._current_fig
    @current_fig.setter
    def current_fig(self, value):
        self._current_fig = value
    @current_fig.deleter
    def current_fig(self):
        del self._current_fig

    @property
    def current_widget(self):
        return self._current_widg
    @current_widget.setter
    def current_widget(self, value):
        self._current_widg = value
    @current_widget.deleter
    def current_widget(self):
        del self._current_widg

    def add_axis(self, name, arange, atype='y', location='right',
                 idx=None, iframe=None, *args, **kwargs):
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.add_axis(name, arange, atype, location, _idx, _iframe, *args, **kwargs)
        self._set_current_indexes(_idx, _iframe)
        
    def add_figure(self, iframe=0):
        """Adds figure to the specified frame"""
        self._stack.add_figure(iframe)
        self._current_fig['figure'] = self.get_nfigs(iframe) - 1

    def add_frame(self, name, nfigs=1):
        self.check_html(name)
        self._filenames.append(name)
        self._stack.add_frame(nfigs)
        self._current_fig['frame'] = self.get_nframes() - 1
        self._current_fig['figure'] = 0

    def add_widget(self, iframe=0):
        """Adds widget to the specified frame"""
        self._stack.add_widget(iframe)
        self._current_fig['widget'] = self.get_nwidgets(iframe) - 1

    def box(self, x, y, idx=None, iframe=None, *args, **kwargs):
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.box(x, y, _idx, _iframe, *args, **kwargs)
        self._set_current_indexes(_idx, _iframe)
    
    def boxplot(self, data=None, style='vertical', idx=None, iframe=None,
                outliers_factor=1.5, outliers_color='grey',
                *args, **kwargs):
        """
        Create box plots. 
        If more than one is intended, `args` and `kwargs` apply to all, 
        and `idx` can be specified as a list. 
        The data has to be composed of two numpy arrays with the same 
        length organized in a list [np.array(1), np.array(2)]:
            - 1: the variable which distribution is going to be shown
            - 2: the variable to be used for the splitting
        """
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.boxplot(data, style, _idx, _iframe, outliers_factor=outliers_factor, outliers_color=outliers_color, *args, **kwargs)
        self._set_current_indexes(_idx, _iframe)

    def check_html(self, name):
        if '.' in name:
            if name[-4:] != 'html':
                raise ValueError("Only '.html' files are supported")

    def _frame_to_matrix(self, nrows, ncols, iframe):
        if nrows*ncols < self.get_nfigs(iframe=iframe):
            warnings.warn('The matrix does not include all {} figures of frame {}'.format(self.get_nfigs(iframe), iframe), UserWarning)
        return self._stack.frame_to_matrix(nrows, ncols, iframe)

    def fit(self, p0, pdf, idx=None, iframe=None, *args, **kwargs):
        """Fits the object stored in the figure pointed at with (idx,iframe)"""
        _idx, _iframe = self._get_current_indexes(idx, iframe)
        return self._stack.fit(p0, pdf, _idx, _iframe, *args, **kwargs)

    def _get_current_indexes(self, idx=None, iframe=None):
        """Returns the indexes of the figure according to `self._current_fig` or to the user"""
        _iframe = self._current_fig['frame'] if iframe is None else iframe
        nframes = self.get_nframes()
        if _iframe > nframes:
            raise ValueError('Frame {} was requested (0-indexed) while only {} were allocated.'.format(_iframe, nframes))
        _idx = self._current_fig['figure']+1 if idx is None else idx
        nfigs = self.get_nfigs(_iframe)
        if isinstance(_idx, (tuple,list)):
            if any(_idx) > nfigs:
                raise ValueError('One of the figures in {} has an index (0-based) larger than {}, the number of figures that were allocated.'.format(_idx, nfigs))
        else:
            if _idx > nfigs:
                raise ValueError('Figure {} was requested (0-indexed) while only {} were allocated.'.format(_idx, nfigs))
        return _idx, _iframe    

    def _get_current_multiple_indexes(self, idx=None, iframe=None):
        if isinstance(idx, (tuple, list)):
            _, _iframe = self._get_current_indexes(idx, iframe)
            return idx, _iframe
        else:
            return self._get_current_indexes(idx, iframe)

    def get_figure(self, idx=None, iframe=None):
        """Returns bokeh.plotting.figure() underlying object"""
        _idx, _iframe = self._get_current_indexes(idx, iframe)
        return self._stack.get_figure(_idx, _iframe)

    def get_nfigs(self, iframe):
        """Returns number of rows in a frame"""
        return self._stack.get_nfigs(iframe)

    def get_nframes(self):
        """Returns number frames in the stack"""
        return self._stack.get_nframes()

    def get_nwidgets(self, iframe):
        """Returns number of widgets in a frame"""
        return self._stack.get_nwidgets(iframe)

    def get_widget(self, idx=None, iframe=None):
        """Returns bokeh.plotting.figure() underlying object"""
        _iframe = self._current_widg['frame'] if iframe is None else iframe
        _iw = self._current_widg['widget'] if idx is None else idx
        self._current_widg['widget'] = _iw
        self._current_widg['frame'] = _iframe
        return self._stack.get_widget(_iw, _iframe)

    def graph(self, data=None, errors=None, style='circle', idx=None, iframe=None, *args, **kwargs):
        """Create 1D graphs. If more than one graph, `args` and `kwargs` apply to all, and `idx` can be specified as a list."""
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.graph(data=data, errors=errors, style=style, idx=_idx, iframe=_iframe, **kwargs)
        self._set_current_indexes(_idx, _iframe)

    def histogram(self, data, style=None, idx=None, iframe=None, *args, **kwargs):
        """Create 1D histograms. If more than one histogram, `args` and `kwargs` apply to all, and `idx` can be specified as a list."""
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.histogram(data, style, _idx, _iframe, **kwargs)
        self._set_current_indexes(_idx, _iframe)

    def line(self, x, y, idx=None, iframe=None, *args, **kwargs):
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.line(x, y, _idx, _iframe, *args, **kwargs)
        self._set_current_indexes(_idx, _iframe)

    def label(self, label, x, y, idx=None, iframe=None, *args, **kwargs):
        _idx, _iframe = self._get_current_multiple_indexes(idx, iframe)
        self._stack.label(label, x, y, _idx, _iframe, *args, **kwargs)
        self._set_current_indexes(_idx, _iframe)

    def put(self, figures, idx=None, iframe=0):
        if iframe >= self.get_nframes():
            raise ValueError('The specified frame was not allocated.')
        if idx is None:
            raise ValueError('Please specify where you want to put the figure. By default, the first frame (`iframe` = 0) is used.')
        if isinstance(iframe, (tuple,list)):
            raise ValueError('Put figures in one frame at a time.')

        if isinstance(figures, (tuple,list)):
            if not isinstance(idx, (tuple,list)):
                raise TypeError('When providing multiple figures, multiple indexes must be provided.')
            for i,j in enumerate(idx):
                self.stack_.set_figure_to(figures[i], j, i)
        else:
            self.stack_.set_figure_to(figures, idx, iframe)
            self._current_fig['frame'] = iframe
            self._current_fig['figure'] = idx
        
    def save_fig(self, filename=None, path=None, iframe=None, idx=None, mode='png', *args, **kwargs):
        if mode not in ('png',):
            raise ValueError('BokehPlot.save_fig(): only PNG pictures can be produced for the moment.')
        idx, iframe = self._get_current_indexes(idx=idx, iframe=iframe)
        if filename is None:
            if '.' in os.path.basename(self._filenames[iframe]):
                filename = os.path.splitext(self._filenames[iframe])[0]
            else:
                filename = self._filenames[iframe]
            filename += '_' + str(idx).zfill(4) + '.' + mode #a frame should likely not have more than 1000 figures!
        if path is not None:
            filename = os.path.basename(filename)
            filename = os.path.join(path, filename)
        print( "Picture saved in ", filename )
        export_png( self.get_figure(idx=idx, iframe=iframe), filename=filename )

    def save_figs(self, filename=None, path=None, iframe=None, mode='png', *args, **kwargs):
        _, iframe = self._get_current_indexes(idx=0, iframe=iframe)
        for idx in range(self.get_nfigs(iframe)):
            self.save_fig(filename=filename, path=path, iframe=iframe, idx=idx, mode=mode, *args, **kwargs)
    
    def save_frame(self, iframe=None, nrows=None, ncols=None, show=False, layout=None, *args, **kwargs):
        """Displays frame using bokeh.show() or bokeh.save() and bokeh.gridplot() or bokeh.layout().
        The argument 'layout', if provided, should contain figure indexes in a list of lists, ordered by rows."""
        _, _iframe = self._get_current_indexes(idx=0, iframe=iframe)
        if '.' in self._filenames[_iframe]:
            output_file(filename=self._filenames[_iframe])
        else:
            output_file( self._filenames[iframe] + '.html' )

        if nrows is not None or ncols is not None:
            if layout is not None:
                raise error_handler('value', basename(__file__),
                                    "You cannot specify 'nrows'/'ncols' and 'layout' simultaneously.")

            if nrows is not None and ncols is not None:
                display = gridplot( self._frame_to_matrix(nrows, ncols, _iframe) )
            elif nrows is None and ncols is not None:
                nfigs = self.get_nfigs(_iframe)
                nrows = int(np.ceil(nfigs/ncols))
                display = gridplot( self._frame_to_matrix(nrows, ncols, _iframe) )
            elif nrows is not None and ncols is None:
                nfigs = self.get_nfigs(_iframe)
                ncols = int(np.ceil(nfigs/nrows))
                display = gridplot( self._frame_to_matrix(nrows, ncols, _iframe) )
                
        else:
            display = [] 
            if layout:
                if not all([isinstance(l,list) for l in layout]):
                    raise error_handler('type', basename(__file__),
                                        "'layout' should be a list of lists.")
                for l in layout:
                    display.append([])
                    for idx in l:
                        display[-1].append( self.get_figure(idx, _iframe) )
            else:
                nfigs = self.get_nfigs(_iframe)
                nwidgs = self.get_nwidgets(_iframe)
                nbaskets = nfigs + nwidgs
                nrows = int(np.sqrt(nbaskets))+1 #guarantees there are enough rows and columns
                ncols = nrows
                for i in range(nrows):
                    if i*ncols >= nbaskets: #all baskets (figures+objects) have already been plotted
                        break
                    display.append([])
                    for j in range(ncols):
                        ibasket = i*ncols + j
                        if ibasket >= nwidgs: # figure
                            ifig = ibasket-nwidgs
                            if self._stack.figure_exists(ifig, _iframe):
                                display[i].append( self.get_figure(ifig, _iframe) )
                        else: # widget
                            iwidget = ibasket
                            if self._stack.widget_exists(iwidget, _iframe):
                                display[i].append( self.get_widget(iwidget, _iframe) )
                display = [x for x in display if x != []] #removes potential empty lists
            display = bokehlayout( display )
        if show:
            bokehshow( display )
        else:
            save( display )

    def save_frames(self, nrows=None, ncols=None, show=False, *args, **kwargs):
        """Save all the frames at once.

        Args: nrows, ncols: number of rows and columns of frame to be displayed
              show: works in conjunction with `bokeh serve`
        """
        for i in range(self.get_nframes()):
            self.save_frame(i, nrows, ncols, show, *args, **kwargs)

    def _set_current_indexes(self, idx, iframe):
        if isinstance(idx, (tuple,list)):
            idx = idx[0]
        self._current_fig['figure'] = idx
        self._current_fig['frame'] = iframe

    def widget(self, widget_type, callback, link_to_figures, link_to_objects=[[0]],
               idx=None, iframe=None, **kwargs):
        """
        Creates a widget and links it to data in some figures and their objects.

        Args: 'widget_type': which widget to use
              'callback': full JavaScript code for the callback as a string
              'link_to_figures': list of figures' indexes to consider in the callback
              'link_to_objects': list of lists of objects' indexes to consider in the callback,
                                 one list per figure, ordered according to 'link_to_figures'
        """        
        if ( not isinstance(link_to_figures, (tuple, list)) or
             not isinstance(link_to_objects, (tuple, list)) or
             not isinstance(link_to_objects[0], (tuple, list)) ):
            raise ValueError('Some widget links have the wrong type.')
            
        _iframe = self._current_widg['frame'] if iframe is None else iframe
        _iw = self._current_widg['widget'] + 1 if idx is None else idx
        self._stack.widget(widget_type, callback, link_to_figures, link_to_objects,
                           _iw, _iframe, **kwargs)
        self._current_widg['widget'] = _iw
        self._current_widg['frame'] = _iframe

