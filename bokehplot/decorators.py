import functools

def fig_kwargs_manager(func):
    """Decorator for keeping 'fig_kwargs' only when required"""
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        self._fig_kwargs = None if 'fig_kwargs' not in kwargs else kwargs['fig_kwargs']
        if self._fig_kwargs is not None:
            kwargs.pop('fig_kwargs')
        func(self, *args, **kwargs)
        self._set_fig_kwargs()
    return wrapper
