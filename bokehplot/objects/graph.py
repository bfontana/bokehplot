__all__ = ['Graph1D', 'Graph2D']

import os
import numpy as np
from scipy.optimize import curve_fit

from bokehplot.objects.objects import Graph
from bokehplot.utils.misc import draw_color_bar, select_style, find_nearest_color
from bokehplot.utils.misc import error_handler, kw_lists, basename
from bokehplot.utils.math import MathFunctions

import bokeh.palettes as palettes
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource
from bokeh.models.glyphs import Line

class Graph1D(Graph):
    def __init__(self, fig, x, y,
                 ex_down, ex_up, ey_down, ey_up, style, line=False, **kwargs):
        """1D graph class of BokehPlot"""
        super(Graph1D, self).__init__(figure=fig, x=x, y=y, z=None,
                                      ex_down=ex_down, ex_up=ex_up, ey_up=ey_up, ey_down=ey_down,
                                      style=style)
        self._source = self.create_source(**kwargs)

        if line:
            _items_to_remove = []
            line_kwargs = {}
            line_prop_list = kw_lists('line')

            line_prop_list.extend(self.common_kwargs)
            for prop in kwargs.keys():
                if prop in line_prop_list:
                    if 'line_' in prop or prop in self.common_kwargs:
                        line_kwargs.update({prop:kwargs[prop]})
                    if 'line_' in prop and prop not in self.common_kwargs:
                        _items_to_remove.append(prop)
            if 'line_color' not in line_kwargs:
                #Defaults:
                line_kwargs['line_color']=kwargs['color']
            self._figure.line(x=self.x, y=self.y, **line_kwargs)
            kwargs = {k:v for k,v in kwargs.items() if k not in _items_to_remove}
            
        if self.exu is not None:
            whisker_props_list = ['whisker_length','whisker_width',]
            whisker_props_list.extend(self.common_kwargs)
            whisker_kwargs = {}
            for prop in kwargs.keys():
                if 'whisker_' in prop and prop not in whisker_props_list:
                    message = ( '{} is not a recognised whisker property. Choose one of the following: {}'
                                .format(prop, whisker_props_list) )
                    raise error_handler('value', __file__, message)
            for prop in whisker_props_list:
                if 'whisker_' in prop:
                    if prop in kwargs.keys():
                        whisker_kwargs.update({prop:kwargs[prop]})
                        kwargs.pop(prop)
        width_factor = 1.5
        _tmp = list( width_factor*(self.x[1:]-self.x[:-1])/2 )
        if len(_tmp)==0:
            widths = 0.9
        else:
            widths = [_tmp[0]]
            widths.extend(_tmp)

        whisker_size = 4
        if style in ('vbar', 'vbar_stack'):
            getattr(self._figure, style)(self.x, widths, self.y, **kwargs)
        elif style.split('%')[0] in ('vbar', 'vbar_stack'):
            st, texture, weight, color = style.split('%')
            getattr(self._figure, st)(x=self.x, top=self.y, width=widths, 
                                      hatch_pattern=texture, hatch_weight=float(weight), hatch_color=color, **kwargs)
        else:
            if '%' in style:
                raise ValueError("BokehPlot.Graph1D: only one item can be used for the graph's style when it is not a `vbar` or a `vbar_stack`.")
            getattr(self._figure, style)(x='x', y='y', source=self._source, **kwargs)
            whisker_size = 4 if 'size' not in kwargs else kwargs['size'] #bokeh default for marker size is 4

        if self.exu is not None:
            seg_kwargs = {}
            if line: # define error bar properties based on line properties
                seg_kwargs.update({k:v for k,v in line_kwargs.items() if k in self.common_line_seg_kwargs})
                # if 'line_width' in line_kwargs:
                #     seg_kwargs['line_width'] = line_kwargs['line_width']
                # if 'line_color' in line_kwargs:
                #     seg_kwargs['line_color'] = line_kwargs['line_color']
            for prop in self.common_kwargs:
                if prop in kwargs.keys():
                    seg_kwargs[prop] = kwargs[prop]

                    #In order: left horizontal, right horizontal, lower vertical and upper vertical stems; upper and lower whiskers
            for i in range(len(self.x)):
                self._figure.segment(x0=self.x[i]-self.exd[i], x1=self.x[i], y0=self.y[i], y1=self.y[i], **seg_kwargs)
                self._figure.segment(x0=self.x[i], x1=self.x[i]+self.exu[i], y0=self.y[i], y1=self.y[i], **seg_kwargs)
                self._figure.segment(x0=self.x[i], x1=self.x[i], y0=self.y[i], y1=self.y[i]-self.eyd[i], **seg_kwargs)
                self._figure.segment(x0=self.x[i], x1=self.x[i], y0=self.y[i], y1=self.y[i]+self.eyu[i], **seg_kwargs)

                # whiskers
                wlen = ( whisker_size/3 if whisker_props_list[0] not in whisker_kwargs
                         else whisker_kwargs[whisker_props_list[0]] )
                if 'whisker_width' in whisker_kwargs.keys():
                    seg_kwargs.update({'line_width': whisker_kwargs['whisker_width']})
                self._figure.segment(x0=self.x[i]-wlen/2., x1=self.x[i]+wlen/2.,
                                     y0=self.y[i]+self.eyu[i], y1=self.y[i]+self.eyu[i], **seg_kwargs)
                self._figure.segment(x0=self.x[i]-wlen/2., x1=self.x[i]+wlen/2.,
                                     y0=self.y[i]-self.eyd[i], y1=self.y[i]-self.eyd[i], **seg_kwargs)

    def fit(self, *args, **kwargs):
        """Fit Graph1D with provided or predefined PDF"""
        pdf = kwargs['pdf']
        if isinstance(pdf, str):
            func = MathFunctions.create(pdf)
            return curve_fit(func.func, self.x, self.y, p0=kwargs['p0'], bounds=func.bounds)
        else:
            return curve_fit(pdf, self.x, self.y, p0=p0)

    def create_source(self, **kwargs):
        l = kw_lists('source_params')
        #add extra parameters to data source!
        if self._are_errors_defined:
            cds = ColumnDataSource(data=dict(x=self.x, y=self.y,
                                             exd=self.exd, exu=self.exu, eyd=self.eyd, eyu=self.eyu))
        else:
            cds = ColumnDataSource(data=dict(x=self.x.tolist(), y=self.y.tolist()))
        return cds

class Graph2D(Graph):
    def __init__(self, fig, x, y, z,
                 ex_down, ex_up, ey_down, ey_up, style,
                 continuum_value=None, continuum_color=None, **kwargs):
        """2D graph class of BokehPlot"""
        super(Graph2D, self).__init__(figure=fig, x=x, y=y, z=x,
                                      ex_down=ex_down, ex_up=ex_up, ey_up=ey_up, ey_down=ey_down,
                                      style=style)
        self.x, self.y, self.counts = x, y, z
        self._figure = fig
        self.style = style
        assert(len(self.x) == len(self.y) == len(self.counts))
        self.kwargs = kwargs
        self.continuum_value = continuum_value
        self.continuum_color = continuum_color

        try:
            if self.style in ('vbar', 'vbar_stack'):
                raise ValueError('Bars in a 2D graph do not make much sense (for the moment, who knows!).')
            elif self.style.split('%')[0] in ('vbar', 'vbar_stack'):
                raise ValueError('Bars in a 2D graph do not make much sense (for the moment, who knows!).')
            else:
                if 'scale' in kwargs.keys():
                    self.create_graph(scale=kwargs['scale'])
                else:
                    self.create_graph()
        except AttributeError:
            raise AttributeError('The attribute does not exist. Perhaps the style you mentioned does not correspond to any method of `BokehPlot.figure()`, or maybe you tried to specify kwargs of the figure object without using the `fig_kwargs` argument?')

    def create_graph(self, scale='linear'):
        """Creates a 2D graph, including the color bar."""
        st, texture, weight, palette = select_style(self.style)
        count_max, count_min = np.max(self.counts), np.min(self.counts)
        count_to_color = np.linspace(count_min, count_max, len(palette)+1)
        for entry in range(len(self.counts)):
            c = find_nearest_color(count_to_color, self.counts[entry], palette)
            if self.continuum_value is not None and self.continuum_color is not None:
                c = self.set_continuum(self.counts[entry], c)
            if st == 'rect' and ('width' not in self.kwargs.keys() or 'height' not in self.kwargs.keys()):
                raise ValueError("Pass the width and height options when using the 'rect' style.")
            getattr(self._figure, st)(self.x[entry], self.y[entry], color=c, **self.kwargs)
        draw_color_bar(self._figure, palette, count_min, count_max, scale)

    def set_continuum(self, count, color):
        """Changes palette color to `self.continuum_color` when the count number equals `self.continuum_count`"""
        if isinstance(count, (tuple,list)):
            assert(len(self.continuum_color) == len(self.continuum_value))
            for val,col in zip(self.continuum_value,self.continuum_color):
                if count == val:
                    return col
        else:
            if count == self.continuum_value:
                return self.continuum_color
        return color

    def create_source(self, **kwargs):
        pass
