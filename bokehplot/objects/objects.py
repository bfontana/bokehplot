__all__ = ['Object', 'Graph']

import abc
from bokehplot.utils.misc import error_handler, basename, kw_lists
from bokehplot.utils.data_analysis import convert_list_to_numpy

class Object(abc.ABC):
    """
    Base class for BokehPlot objects (everything which can be plotted in a BokehPlot.Figure).
    """
    def __init__(self):
        self._source = None

    @property
    def source(self):
        return self._source

    @abc.abstractmethod
    def create_source(self, **kwargs):
        raise NotImplementedError()

    def create_source_params_dict(self, length, **kwargs):
        """
        Creates two dicts which allow interaction with potentially all elements passed to an object,
        like colors and sizes, for instance.
        The data is hardcoded into the functions, but all the rest must be generalized. This method
        creates the required info to make interaction work when using ColumnDataSources.

        Args: - length: length of the columns in this object's ColumnDataSource
              
        Returns: - dict of keys, to be passed to the object creation methods, such as figure.circles()
                 - dict of values, to be passed to the ColumnDataSource.
        """
        kwl = kw_lists('source_params')
        d, keys = (dict() for _ in range(2))
        #add extra parameters to data source!
        for l in kwl:
            if l in kwargs.keys():
                keys.update({l: l})
                if not isinstance(kwargs[l], (tuple,list)):
                    d.update({l: [kwargs[l] for _ in range(length)] })
                else:
                    d.update({l: kwargs[l]})
        return keys, d

class Graph(Object):
    def __init__(self, figure, x, y, z,
                 ex_down, ex_up, ey_down, ey_up, style, **kwargs):
        self.x = convert_list_to_numpy(x)
        self.y = convert_list_to_numpy(y)
        self.z = convert_list_to_numpy(z)
        self.exd, self.exu, self.eyd, self.eyu = ex_down, ex_up, ey_down, ey_up
        self._are_errors_defined = self._are_errors_defined_()
        self._figure = figure
        self._source_kwdict = self.create_source_params_dict(len(self.x), **kwargs)
        self.common_line_seg_kwargs = ('line_color', 'line_width',)
        self.common_kwargs = ['y_range_name',]

    def _are_errors_defined_(self):
        if ( self.exd is not None or self.exu is not None or
             self.eyd is not None or self.exu is not None ):
            if not not [x for x in (self.exd, self.exu, self.eyd, self.eyu) if x is None]:
                raise error_handler('type', basename(__file__), 'Some errors were not defined.')
            else:
                return True
        else:
            return False

    def create_source(self, **kwargs): #is the NotImplementedError seen by Graph1D and Graph2D?
        pass

class Histogram(Object):
    def __init__(self, figure, data, ey_down, ey_up, style, **kwargs):
        self.eyd, self.eyu = ey_down, ey_up
        self._are_errors_defined = self._are_errors_defined_()
        self._figure = figure
        
    def _are_errors_defined_(self):
        if self.eyd is not None or self.eyu is not None:
            if not not [x for x in (self.eyd, self.eyu) if x is None]:
                raise error_handler('type', basename(__file__), 'Some errors were not defined.')
            else:
                return True
        else:
            return False
        
    def create_source(self, **kwargs): #is the NotImplementedError seen by Histogram1D and Histogram2D?
        pass
