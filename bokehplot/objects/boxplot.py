__all__ = ['Box1D']

from bokeh.plotting import figure
from bokehplot.utils.misc import draw_color_bar, select_style, find_nearest_color
from bokehplot.utils.math import MathFunctions
import bokeh.palettes as palettes
from scipy.optimize import curve_fit
from bokeh.models import Range1d
import numpy as np

class BoxPlot(object):
    def __init__(self, figure, data, style, outliers_factor, outliers_color):
        self._figure = figure

class BoxPlot1DVertical(BoxPlot):
    def __init__(self, figure, data, style, outliers_factor, outliers_color, *args, **kwargs):
        if len(data)!=2:
            raise TypeError('BokehPlot.BoxPlot1DVertical: The data do not have the right type.')
        if type(data[0]) != np.ndarray and type(data[0][0]) != np.ndarray:
            raise TypeError('BokehPlot.BoxPlot1DVertical: The data do not have the right type.')
        super(BoxPlot1DVertical, self).__init__(figure=figure, data=data, style=style, 
                                        outliers_factor=outliers_factor, outliers_color=outliers_color)

        self.x, self.categories = data[0], data[1]
        if isinstance(self.categories[0],str):
            self.categories = np.array(self.categories)
        uniqcats = np.unique(self.categories)
        q25, median, q75, upper, lower = ([] for _ in range(5))
        outliersx, outliersy = ([] for _ in range(2))
        for cat in uniqcats:
            d = self.x[ self.categories==cat ]
            q25.append( np.quantile(d, 0.25) )
            median.append( np.quantile(d, 0.50) )
            q75.append( np.quantile(d, 0.75) )
            interquart_dist = q75[-1] - q25[-1]
            upper.append( q75[-1] + outliers_factor*interquart_dist )
            lower.append( q25[-1] - outliers_factor*interquart_dist )
            outliersy.append( d[ (d<lower[-1]) | (d>upper[-1]) ] )

        c  = kwargs.setdefault('color', 'black')
        lc = kwargs.setdefault('line_color', 'black')
        uniqcats_tickers = [i for i in range(len(uniqcats))]
        for i,cat_tick in enumerate(uniqcats_tickers):
            outliersx.append( cat_tick*np.ones(len(outliersy[i])) )
        #stems
        self._figure.segment(uniqcats_tickers, upper, uniqcats_tickers, q75, line_color=lc, line_width=2)
        self._figure.segment(uniqcats_tickers, lower, uniqcats_tickers, q25, line_color=lc, line_width=2)
        #boxes
        self._figure.vbar(uniqcats_tickers, 0.7, median, q75, fill_color=c, line_color=lc, line_width=2)
        self._figure.vbar(uniqcats_tickers, 0.7, q25, median, fill_color=c, line_color=lc, line_width=2)
        #whiskers
        self._figure.rect(uniqcats_tickers, lower, 0.2, 0.01, line_color=lc)
        self._figure.rect(uniqcats_tickers, upper, 0.2, 0.01, line_color=lc)
        # outliers
        if len(outliersx[0]) != 0:
            for outx,outy in zip(outliersx,outliersy):
                self._figure.diamond(outx, outy, size=6, color=outliers_color, fill_alpha=0.6)
                
        labels = [str(u) for u in uniqcats]
        self._figure.xaxis.ticker = uniqcats_tickers
        self._figure.xaxis.major_label_overrides = dict((i,u) for i,u in zip(uniqcats_tickers,labels) )

class BoxPlot1DHorizontal(BoxPlot):
    def __init__(self, figure, data, style, outliers_factor, outliers_color, *args, **kwargs):
        if len(data)!=2:
            raise TypeError('BokehPlot.BoxPlot1DHorizontal: The data do not have the right type.')
        if type(data[1]) != np.ndarray and type(data[1][0]) != np.ndarray:
            raise TypeError('BokehPlot.BoxPlot1DHorizontal: The data do not have the right type.')
        super(BoxPlot1DHorizontal, self).__init__(figure=figure, data=data, style=style, 
                                                  outliers_factor=outliers_factor, outliers_color=outliers_color)

        self.y, self.categories = data[1], data[0]
        if isinstance(self.categories[0],str):
            self.categories = np.array(self.categories)
        uniqcats = np.unique(self.categories)
        q25, median, q75, left, right = ([] for _ in range(5))
        outliersx, outliersy = ([] for _ in range(2))
        for cat in uniqcats:
            d = self.y[ self.categories==cat ]
            q25.append( np.quantile(d, 0.25) )
            median.append( np.quantile(d, 0.50) )
            q75.append( np.quantile(d, 0.75) )
            interquart_dist = q75[-1] - q25[-1]
            right.append( q75[-1] + outliers_factor*interquart_dist )
            left.append( q25[-1] - outliers_factor*interquart_dist )
            outliersx.append( d[ (d<left[-1]) | (d>right[-1]) ] )

        c  = kwargs.setdefault('color', 'black')
        lc = kwargs.setdefault('line_color', 'black')
        uniqcats_tickers = [i for i in range(len(uniqcats))]
        for i,cat_tick in enumerate(uniqcats_tickers):
            outliersy.append( cat_tick*np.ones(len(outliersx[i])) )
        #stems
        self._figure.segment(left, uniqcats_tickers, q25, uniqcats_tickers, line_color=lc, line_width=2)
        self._figure.segment(q25, uniqcats_tickers, right, uniqcats_tickers, line_color=lc, line_width=2)
        #boxes
        self._figure.hbar(uniqcats_tickers, 0.7, median, q25, fill_color=c, line_color=lc, line_width=2)
        self._figure.hbar(uniqcats_tickers, 0.7, q75, median, fill_color=c, line_color=lc, line_width=2)
        #whiskers
        self._figure.rect(left, uniqcats_tickers, 0.01, 0.2, line_color=lc)
        self._figure.rect(right, uniqcats_tickers, 0.01, 0.2, line_color=lc)
        # outliers
        if len(outliersx[0]) != 0:
            for outx,outy in zip(outliersx,outliersy):
                self._figure.diamond(outx, outy, size=6, color=outliers_color, fill_alpha=0.6)
                
        labels = [str(u) for u in uniqcats]
        self._figure.yaxis.ticker = uniqcats_tickers
        self._figure.yaxis.major_label_overrides = dict((i,u) for i,u in zip(uniqcats_tickers,labels) )
