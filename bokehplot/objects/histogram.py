__all__ = ['Histogram1D', 'Histogram2D']

from bokeh.plotting import figure
from bokehplot.utils.misc import draw_color_bar, select_style, find_nearest_color
from bokehplot.utils.math import MathFunctions
from scipy.optimize import curve_fit
import numpy as np

from bokehplot.objects.objects import Histogram

class Histogram1D(Histogram):
    def __init__(self, fig, data, style,
                 ey_down=None, ey_up=None, *args, **kwargs):
        super(Histogram1D, self).__init__(figure=fig, data=data,
                                          ey_down=ey_down, ey_up=ey_up, style=style)
        self.y, self.bin_edges = data
        self.x = (self.bin_edges[:-1] + self.bin_edges[1:])/2
        if style:
            if style == 'step': #step histogram
                #line_color = kwargs.setdefault('line_color', kwargs.setdefault('color', 'black'))
                self._figure.step(x=(self.bin_edges[:-1]+self.bin_edges[1:])/2, y=self.y, **kwargs)
            else: #texture filled histogram
                if '%' not in style:
                    raise ValueError('When defining textures, `style` must be specified as follows: <texture>%<weight>%<color>')
                texture, weight, color = style.split('%')
                self._figure.quad(top=self.y, bottom=0, left=self.bin_edges[:-1], right=self.bin_edges[1:], 
                                  hatch_pattern=texture, hatch_weight=float(weight), hatch_color=color, *args, **kwargs)

        else: #normal histogram
            self._figure.quad(top=self.y, bottom=0, left=self.bin_edges[:-1], right=self.bin_edges[1:], *args, **kwargs)
        
    def fit(self, *args, **kwargs):
        pdf = kwargs['pdf']
        if isinstance(pdf, str):
            func = MathFunctions.create(pdf)
            return curve_fit(func.func, self.x, self.y, p0=kwargs['p0'], bounds=func.bounds)
        else:
            return curve_fit(pdf, self.x, self.y, p0=p0)

class Histogram2D(Histogram):
    def __init__(self, fig, data, style, ey_down=None, ey_up=None, continuum_value=None, continuum_color=None, colorbar_limits=(None,None), *args, **kwargs):
        super(Histogram2D, self).__init__(figure=fig, data=data, style=style, ey_down=ey_down, ey_up=ey_up)
        self.hdata, self.bin_edges_x, self.bin_edges_y = data
        self.style = style
        self.args = args
        self.kwargs = kwargs
        self.continuum_value = continuum_value
        self.continuum_color = continuum_color
        self.colorbar_limits = colorbar_limits
        if 'scale' in kwargs:
            self.scale = kwargs['scale']
            kwargs.pop('scale')
        else:
            self.scale = 'linear'

        if style:
            if 'quad' not in style:
                raise ValueError('You must specify `style=quad` for a standard 2d histogram. In the future other types might be added.')
            elif 'quad' in style:
                self.quad_histogram()
        else: #normal 2D histogram
            self.quad_histogram()

    def quad_histogram(self):
        _, texture, weight, palette = select_style(self.style)
        count_min = np.min(self.hdata) if self.colorbar_limits[0] is None else self.colorbar_limits[0]
        count_max = np.max(self.hdata) if self.colorbar_limits[1] is None else self.colorbar_limits[1]
        count_to_color = np.linspace(count_min, count_max, len(palette)+1)
        hedgesleft, hedgesright, vedgestop, vedgesbot, colors = ([] for _ in range(5))

        for i in range(len(self.hdata)):
            lendimx = len(self.hdata[i])
            for j in range(lendimx):
                c = find_nearest_color(count_to_color, self.hdata[i][j], palette)
                if self.continuum_value is not None and self.continuum_color is not None:
                    c = self.set_continuum(self.hdata[i][j], c)
                colors.append(c)
            vedgestop.extend(self.bin_edges_y[1:])
            vedgesbot.extend(self.bin_edges_y[:-1])
            hedgesleft.extend( [self.bin_edges_x[i] for _ in range(lendimx)] )
            hedgesright.extend( [self.bin_edges_x[i+1] for _ in range(lendimx)] )

        self._figure.quad(top=vedgestop, bottom=vedgesbot, left=hedgesleft, right=hedgesright, hatch_pattern=texture, hatch_weight=weight, color=colors, *self.args, **self.kwargs)
        draw_color_bar(self._figure, palette, count_min, count_max, scale=self.scale)

    def set_continuum(self, count, color):
        """Changes palette color to `self.continuum_color` when the count number equals `self.continuum_count`"""
        if isinstance(self.continuum_value, (tuple,list)):
            assert(len(self.continuum_color) == len(self.continuum_value))
            for val,col in zip(self.continuum_value,self.continuum_color):
                if count == val:
                    return col
        else:
            if count == int(self.continuum_value):
                return self.continuum_color
        return color
