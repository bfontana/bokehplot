__all__ = ['Widget', 'Button', 'Dropdown']
from bokeh.models import CustomJS, Dropdown as DD, Slider as SL

class Button():
    def __init__(self, callback, **kwargs):
        pass

    def handler(self, event):
        print(event.item)

class Dropdown(Button):
    def __init__(self, callback, **kwargs):
        super().__init__(callback, **kwargs)
        self.dropdown = DD(**kwargs)
        self.dropdown.js_on_event("menu_item_click", CustomJS(code="console.log('dropdown: ' + this.item, this.toString())"))
        self.dropdown.on_click(self.handler)

    @property
    def widget(self):
        return self.dropdown

class Slider():
    def __init__(self, callback, **kwargs):
        self.slider = SL(**kwargs)
        self.slider.js_on_change('value', callback)

    @property
    def widget(self):
        return self.slider


#############################################
class Widget():
    def __init__(self, widget_type, callback, sources, **kwargs):
        """
        Args: 'widget_type': which widget to use
              'callback': full JavaScript code for the callback as a string
              'sources': dict {key: source, ...} with all the required sources.
        """
        self._list = ['dropdown', 'slider']
        self._sources = sources
        self._callback = self.create_custom_callback(callback)
        self._widget = self.selector(widget_type, **kwargs)

    @property
    def widget(self):
        return self._widget
    @widget.setter
    def widget(self, value):
        self._widget = value

    def create_custom_callback(self, s):
        print(self._sources['source0_0'].data)
        print(s)
        return CustomJS(args=self._sources, code=s)

    def selector(self, widget_type, **kwargs):
        if widget_type in self._list:
            return getattr(self, '_' + widget_type)(callback=self._callback, **kwargs)
        else:
            raise ValueError('[Widget: The provided widget type is not currently supported.]')

    def _dropdown(self, callback, **kwargs):
        return Dropdown(callback, **kwargs).widget

    def _slider(self, callback, **kwargs):
        return Slider(callback, **kwargs).widget
