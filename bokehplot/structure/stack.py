__all__ = ['Stack']

import os
import numpy as np
from bokehplot.structure.frame import Frame
from bokehplot.utils.misc import error_handler

#############################
class Stack(object):
    def __init__(self, nframes, nfigs, nwidgets, fig_widths=None, fig_heights=None):
        if type(nfigs) != type(nwidgets):
            raise TypeError('[BokePlot.structure] `nfigs` and `nwidgets` must have the same type.')

        self._stack = self._get_stack(nframes, nfigs, nwidgets, fig_widths, fig_heights)
        self._nframes = nframes

    def _get_stack(self, nframes, nfigs, nwidgets, ws, hs):
        if isinstance(nfigs, (tuple, list)):
            lnfigs_ = len(nfigs)
            if nframes != lnfigs_:
                raise error_handler('value', os.path.basename(__file__),
                                    'mismatch between figure and frame numbers.')
            if isinstance(ws, (tuple,list)):
                if len(ws) != lnfigs or len(hs) != lnfigs:
                    raise error_handler('value', os.path.basename(__file__),
                                        'mismatch between figure and width/height dimensions.')
                
                return [Frame(nfig, nwidg, w, h) for nfig,nwidg,w,h in zip(nfigs,nwidgets,ws,hs)]
            else:
                return [Frame(nfig, nwidg, ws, hs) for nfig,nwidg in zip(nfigs,nwidgets)]
                
        else: #each frame is initialized with the same number of figures
            return [Frame(nfigs, nwidgets, ws, hs) for _ in range(nframes)]

    @property
    def stack(self):
        return self._stack
    @stack.setter
    def stack(self, value):
        self._stack = value
    @stack.deleter
    def stack(self):
        del self._stack

    @property
    def nframes(self):
        return self._nframes
    @nframes.setter
    def nframes(self, value):
        self._nframes = value
    @nframes.deleter
    def nframes(self):
        del self._nframes
        
    def add_axis(self, name, arange, atype, location,
                 idx, iframe, *args, **kwargs):
        if atype not in ('x', 'y'):
            raise error_handler('value', 'The axis type must be `x` or `y`.')
        self._stack[iframe].add_axis(name, arange, atype, location,
                                     idx, *args, **kwargs)
       
    def add_figure(self, iframe):
        """Adds figure to the specified frame"""
        self._stack[iframe].add_figure()

    def add_frame(self, nfigs, fig_width, fig_height):
        if isinstance(nfigs, (tuple, list)):
            if not isinstance(fig_width, (tuple, list)) or not isinstance(fig_width, (tuple, list)):
                raise error_handler('value', os.path.basename(__file__),
                                    'wrong width or height list dimensions.')

        frame = Frame(nfigs, fig_width=fig_width, fig_height=fig_height)
        self._stack.append(frame)
        self._nframes += 1

    def box(self, x, y, idx, iframe, *args, **kwargs):
        if isinstance(idx, (tuple,list)):
            if max(idx) > self.get_nfigs(iframe):
                raise ValueError('You need to allocate more figures for placing {} boxes starting at index {} (frame {} currently has only {}).'.format(len(x), idx, iframe, self.get_nfigs(iframe)))
        self._stack[iframe].box(x=x, y=y, idx=idx, *args, **kwargs)

    def boxplot(self, data, style, idx, iframe, outliers_factor, outliers_color, *args, **kwargs):
        if data is None:
            raise ValueError('Please provide the data to plot.')
        if not isinstance(data, (tuple,list)):
            raise TypeError('Please provide the data within a list with numpy arrays. The first item refers to the distribution to be plotted, and the second one represents the variable used to categorize the data. Example: data=[np.array(1,2,3,4,6,5), np.array(10,20,20,20,10,10)] has six data points with two categories, and it will display two box plots with 3 points each.')
        if isinstance(idx, (tuple,list)):
            if max(idx) > self.get_nfigs(iframe):
                raise ValueError('You need to allocate more figures for placing {} boxplots starting at index {} (frame {} currently has only {}).'.format(length, idx, iframe, self.get_nfigs(iframe)))
        self._stack[iframe].boxplot(data, style, idx, outliers_factor, outliers_color, *args, **kwargs)

    def figure_exists(self, idx, iframe):
        return self._stack[iframe].figure_exists(idx)

    def fit(self, p0, pdf, idx, iframe, *args, **kwargs):
        return self._stack[iframe].fit(p0, pdf, idx, *args, **kwargs)

    def frame_to_matrix(self, nrows, ncols, iframe):
        return self._stack[iframe].frame_to_matrix(nrows, ncols)

    def get_figure(self, idx, iframe):
        return self._stack[iframe].get_figure(idx)

    def get_nfigs(self, iframe):
        """Returns number of rows in a frame"""
        return self._stack[iframe].get_nfigs()

    def get_nframes(self):
        """Returns number frames in the stack"""
        return len(self._stack)

    def get_nwidgets(self, iframe):
        """Returns number of widgets in a frame"""
        return self._stack[iframe].get_nwidgets()

    def get_widget(self, idx, iframe):
        return self._stack[iframe].get_widget(idx)

    def graph(self, data, errors, style, idx, iframe, **kwargs):
        if data is None:
            raise ValueError('Please provide the data to plot.')
        if not isinstance(data, (tuple,list)):
            raise TypeError('Please provide the data within a list. Example: data=[np.array(1,2), np.array(10,20)]')
        if not isinstance(errors, (tuple,list)) and errors is not None:
            raise TypeError('Please provide the errors within a list. Example: errors=[ [np.array(1,2),np.array(0,1)], [np.array(2,1),np.array(0,1)]]')
        if isinstance(idx, (tuple,list)):
            if max(idx) > self.get_nfigs(iframe):
                raise ValueError('You need to allocate more figures for placing {} graphs starting at index {} (frame {} currently has only {}).'.format(length, idx, iframe, self.get_nfigs(iframe)))

        self._stack[iframe].graph(data, errors, style, idx, **kwargs)

    def histogram(self, data, style, idx, iframe, *args, **kwargs):
        if type(data[0]) != np.ndarray: #multiple histograms
            if type(idx)==int:
                idx = [idx for _ in range(idx,len(data)+idx)] #broadcasting
            elif isinstance(idx, (tuple,list)):
                if max(idx) > self.get_nfigs(iframe):
                    raise ValueError('You need to allocate more figures for placing {} histograms starting at index {} (frame {} currently has only {}).'.format(len(data), idx, iframe, self.get_nfigs(iframe)))

            for i,item in zip(idx,data):
                self._stack[iframe].histogram(item, style, i, *args, **kwargs)              
        else:
            self._stack[iframe].histogram(data, style, idx, *args, **kwargs)

    def line(self, x, y, idx, iframe, *args, **kwargs):
        if len(x) != len(y):
            raise ValueError("'x' and 'y' must have the same length.")
        if isinstance(idx, (tuple,list)):
                if max(idx) > self.get_nfigs(iframe):
                    raise ValueError('You need to allocate more figures for placing {} lines starting at index {} (frame {} currently has only {}).'.format(len(x), idx, iframe, self.get_nfigs(iframe)))
        self._stack[iframe].line(x, y, idx, *args, **kwargs)

    def label(self, label, x, y, idx, iframe, *args, **kwargs):
        if isinstance(label, (tuple,list)): #multiple labels
            if type(idx)==int:
                idx = [idx for _ in range(idx,len(label)+idx)] #broadcasting
            elif isinstance(idx, (tuple,list)):
                if max(idx) > self.get_nfigs(iframe):
                    raise ValueError('You need to allocate more figures for placing {} labels starting at index {} (frame {} currently has only {}).'.format(len(label), idx, iframe, self.get_nfigs(iframe)))

            for i,item in zip(idx,label):
                self._stack[iframe].label(item, x, y, i, *args, **kwargs)
        else:
            if isinstance(idx, (tuple,list)):
                raise ValueError('When providing multiple indexes, multiple labels have to be provided as well.')
            self._stack[iframe].label(label, x, y, idx, *args, **kwargs)

    def set_figure_to(self, obj, idx, iframe):
        self._stack[iframe].set_figure_to(obj, idx)

    def widget(self, widget_type, callback, link_to_figures, link_to_objects,
               idx, iframe, **kwargs):
        self._stack[iframe].widget(widget_type, callback, link_to_figures, link_to_objects,
                                   idx, iframe, **kwargs)

    def widget_exists(self, idx, iframe):
        return self._stack[iframe].widget_exists(idx)
