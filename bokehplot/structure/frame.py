__all__ = ['Frame']

import numpy as np
from bokehplot.structure.figure import Figure
from bokehplot.interaction import Widget
from bokehplot.utils.data_analysis import has_many_elements

class Frame(object):
    def __init__(self, nfigs, nwidgets, fig_widths, fig_heights):
        if(nfigs<1):
            raise ValueError('The frame needs at least one figure.')
        self._nfigs = nfigs
        self._nwidgs = nwidgets

        if isinstance(fig_widths, (tuple,list)):
            self._figures = [ Figure(i, width=fig_widths[i], height=fig_heights[i]) for i in range(self._nfigs) ]
        else:
            self._figures = [ Figure(i, width=fig_widths, height=fig_heights) for i in range(self._nfigs) ]
        self._widgets = [ None for _ in range(self._nwidgs) ]
        self.figure_properties_ = ('legend_label', 'fill_color', 'line_color')
        self.label_properties_ = ()

    @property
    def nfigs(self):
        return self._nfigs
    @nfigs.setter
    def nfigs(self, value):
        self._nfigs = value
    @nfigs.deleter
    def nfigs(self):
        del self._nfigs

    @property
    def nwidgets(self):
        return self._nwidgs
    @nwidgets.setter
    def nwidgets(self, value):
        self._nwidgs = value
    @nwidgets.deleter
    def nwidgets(self):
        del self._nwidgs

    @property
    def figures(self):
        return self._figures
    @figures.setter
    def figures(self, value):
        self._figures = value
    @figures.deleter
    def figures(self):
        del self._figures

    @property
    def widgets(self):
        return self._widgets
    @widgets.setter
    def widgets(self, value):
        self._widgets = value
    @widgets.deleter
    def widgets(self):
        del self._widgets

    def add_axis(self, name, arange, atype, location, idx,
                 *args, **kwargs):
        self._select_single_kwarg(idx, kwargs, self.figure_properties_)
        self._figures[idx].add_axis(name, arange, atype, location,
                                    *args, **kwargs)

    def add_figure(self, width=None, height=None):
        """Adds figure to the frame"""
        self._figures.append(Figure(self.nfigs, width=width, height=height))
        self.nfigs += 1

    def _aggregate_sources(self, link_to_figures, link_to_objects):
        sources = dict()
        for i,ifig in enumerate(link_to_figures):
            for iobj in link_to_objects[i]:
                sources.update( {'source' + str(ifig) + '_' + str(iobj):
                                 self._figures[ifig].sources[iobj]} )
        return sources
        
    def box(self, x, y, idx, *args, **kwargs):
        assert(len(x) == len(y))
        if isinstance(x[0], (tuple,list)) and isinstance(y[0], (tuple,list)):
            if type(idx)==int:
                idx = [idx for _ in range(idx,len(x)+idx)] #broadcasting
            for w,i,j in zip(idx,x,y):
                self.line(x=[[i[0],i[0]],[i[0],i[1]],[i[0],i[1]],[i[1],i[1]]], 
                          j=[[j[0],j[1]],[j[0],j[0]],[j[1],j[1]],[j[0],j[1]]], 
                          idx=w, *args, **kwargs)
        else:
            self.line(x=[[x[0],x[0]],[x[0],x[1]],[x[0],x[1]],[x[1],x[1]]], 
                      y=[[y[0],y[1]],[y[0],y[0]],[y[1],y[1]],[y[0],y[1]]], 
                      idx=idx, *args, **kwargs)

    def boxplot(self, data, style, idx, outliers_factor, outliers_color, *args, **kwargs):
        self._select_single_kwarg(idx, kwargs, self.figure_properties_)
        multiple_box_check = type(data[0]) != np.ndarray and type(data[1]) != np.ndarray
        if multiple_box_check: #multiple boxplots
            length = len(data)
            if type(idx)==int:
                idx = [idx for _ in range(idx,length+idx)] #broadcasting
            for i,d in zip(idx,data):
                self._figures[i].boxplot(d, style, outliers_factor, outliers_color, *args, **kwargs)
        else: #one single boxplot
            self._figures[idx].boxplot(data, style, outliers_factor, outliers_color, *args, **kwargs)

    def figure_exists(self, idx):
        """Checks if the figure specifed by its index exists"""
        return idx < self._nfigs
    
    def fit(self, p0, pdf, idx, *args, **kwargs):
        return self._figures[idx].fit(p0, pdf, *args, **kwargs)

    def frame_to_matrix(self, nrows, ncols):
        """Convert list of figures inside a frame to matrix (list of lists)"""
        matrix = []
        for irow in range(nrows):
            matrix.append( [] )
            for icol in range(ncols):
                if (icol+irow*ncols >= len(self._figures)): #the loop passed the number of figures
                    continue
                matrix[irow].append( self._figures[irow*ncols + icol].figure )
        return matrix
    
    def get_figure(self, idx):
        return self._figures[idx].figure

    def get_nfigs(self):
        """Returns number of figures in this frame"""
        return self._nfigs

    def get_nwidgets(self):
        """Returns number of widgets in this frame"""
        return self._nwidgs

    def get_widget(self, idx):
        return self._widgets[idx]

    def graph(self, data, errors, style, idx, **kwargs):
        self._select_single_kwarg(idx, kwargs, self.figure_properties_)
        has_many_graphs = has_many_elements(data[0])
        if errors is not None:
            has_many_graphs = has_many_graphs or has_many_elements(errors[0][0][0])

        if has_many_graphs: #multiple graphs
            length = len(data) if data else len(errors)
            if type(idx)==int:
                idx = [idx for _ in range(idx,length+idx)] #broadcasting

            for i,err in zip(idx,errors):
                self._figures[i].graph(data, err, style, **kwargs)
        else: #one single graph
            self._figures[idx].graph(data, errors, style, **kwargs)
        
    def histogram(self, data, style, idx, *args, **kwargs):
        self._select_single_kwarg(idx, kwargs, self.figure_properties_)
        self._figures[idx].histogram(data, style, *args, **kwargs)

    def line(self, x, y, idx, *args, **kwargs):
        assert(len(x) == len(y))
        if isinstance(x[0], (tuple,list)): #multiple lines
            if type(idx)==int:
                idx = [idx for _ in range(idx,len(x)+idx)] #broadcasting
            for i,itemx,itemy in zip(idx,x,y):
                self._figures[i].line(itemx, itemy, *args, **kwargs)
        else:
            if isinstance(idx, (tuple,list)):
                raise ValueError('When providing multiple indexes, multiple lines have to be provided as well.')
            self._figures[idx].line(x, y, *args, **kwargs)

    def label(self, label, x, y, idx, *args, **kwargs):
        self._select_single_kwarg(idx, kwargs, self.label_properties_)
        self._figures[idx].label(label, x, y, *args, **kwargs)

    def _select_single_kwarg(self, idx, kwargs, props):
        for f in props:
            try:
                if isinstance(kwargs[f], (tuple, list)):
                    if len(kwargs[f])!=1: #avoid crash when user uses a one-element list/tuple
                        kwargs[f] = kwargs[f][idx]                
            except KeyError:
                continue

    def set_figure_to(self, obj, idx):
        self._figures[idx].figure = obj

    def widget(self, widget_type, callback, link_to_figures, link_to_objects,
               idx, iframe, **kwargs):
        sources = self._aggregate_sources(link_to_figures, link_to_objects)
        self._widgets[idx] = Widget(widget_type=widget_type, callback=callback,
                                    sources=sources, **kwargs).widget

    def widget_exists(self, idx):
        """Checks if the figure specifed by its index exists"""
        return idx < self._nwidgs
