__all__ = ['Figure']

import numpy as np
import functools
import math
import re

from bokeh.plotting.figure import Figure as bFigure, figure as bfigure
from bokeh.models import ColumnDataSource, Label, Title, Legend
from bokeh.models import Axis, LinearAxis

from bokehplot.utils.math import MathFunctions
from bokehplot.utils.misc import kw_lists, error_handler
from bokehplot.objects.histogram import Histogram1D, Histogram2D
from bokehplot.objects.graph import Graph1D, Graph2D
from bokehplot.objects.boxplot import BoxPlot1DVertical, BoxPlot1DHorizontal

class Figure(bFigure):
    def __init__(self, index, width, height):
        initial_tools = 'save,box_zoom,lasso_select,pan,wheel_zoom,reset'
        self._figure = bfigure(tools=initial_tools,
                               plot_width=width, plot_height=height)
        self._index = index
        self._sources = []
        self._objects = []
        self._axis_kw = kw_lists('axis')
        self._title_kw = kw_lists('title')
        self._legend_kw = kw_lists('legend')
        self._toolbar_kw = kw_lists('toolbar')
        self._source_params = kw_lists('source_params')
        
    @property
    def figure(self):
        return self._figure
    @figure.setter
    def figure(self, value):
        self._figure = value

    @property
    def sources(self):
        return self._sources
    @sources.setter
    def sources(self, value):
        self._sources = value

    @property
    def index(self):
        return self._index
    @index.setter
    def index(self, value):
        self._index = value

    def add_axis(self, name, arange, atype, location,
                 idx=None, *args, **kwargs):
        if atype == 'y':
            self._figure.extra_y_ranges = {name: arange}
            self._figure.add_layout(LinearAxis(y_range_name=name), location)
        elif atype == 'x':
            self._figure.extra_x_ranges = {name: arange}
            self._figure.add_layout(LinearAxis(x_range_name=name), location)

    def box(self, x, y, *args, **kwargs):
        self._figure.box(x, y, *args, **kwargs)

    def boxplot(self, data, style, outliers_factor, outliers_color, *args, **kwargs):
        if style == 'vertical':
            self._objects.append( BoxPlot1DVertical(figure=self._figure, data=data, style=style, 
                                                    outliers_factor=outliers_factor, outliers_color=outliers_color, *args, **kwargs) )
        elif style == 'horizontal':
            self._objects.append( BoxPlot1DHorizontal(figure=self._figure, data=data, style=style, 
                                                      outliers_factor=outliers_factor, outliers_color=outliers_color, *args, **kwargs) )
        else:
            raise error_handler('value', '`style` can either be horizontal or vertical.')

    def check_excess_2d_(self):
        for obj in self._objects:
            if isinstance(obj, (Graph2D, Histogram2D)):
                raise TypeError("BokehPlot.Figure: You cannot add more than one 2D object to the same figure!.")

    def _extract_axis_key(self, key):
        """Extracts key name from properties including axis index, i.e.:
        x.<id>.<prop> -> <prop>"""
        axis_idx = int(re.findall('.+\.(.+)\..+', key)[0])
        new_key = re.sub('\.'+str(axis_idx)+'\.', '.', key)
        return new_key, axis_idx

    def fig_kwargs_manager(func):
        """Decorator for using and dropping 'fig_kwargs' when required"""
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            fkw = 'fig_kwargs'
            self._fig_kwargs = None if fkw not in kwargs else kwargs[fkw]
            if fkw in kwargs:
                kwargs.pop(fkw)
            func(self, *args, **kwargs)
            self._set_fig_kwargs()
        return wrapper

    def fit(self, p0, pdf, line=True, debug=False, obj_idx=0, *args, **kwargs):
        if not isinstance(self._objects[obj_idx], (Histogram1D,Graph1D)):
            raise TypeError('Only fits to 1D histograms or 1D graphs are currently supported.')

        coeff, var = self._objects[obj_idx].fit(p0=p0, pdf=pdf)
        if debug:
            print('The fit returned the following coefficients: ', coeff)
        min_dist = abs(self._objects[obj_idx].x[1] - self._objects[obj_idx].x[0]) / 100
        x = np.arange(self._objects[obj_idx].x[0], self._objects[obj_idx].x[-1], min_dist)
        x_pdf = MathFunctions.create(pdf).func(x, *coeff)
        if line:
            self.line(x, x_pdf, *args, **kwargs)
        return coeff, var

    @fig_kwargs_manager
    def graph(self, data, errors, style, *args, **kwargs):
        if len(data) == 2: #1d graph
            if errors is None:
                obj = Graph1D(self._figure, data[0], data[1],
                              None, None, None, None,                              
                              style, **kwargs)
            else:
                obj = Graph1D(self._figure, data[0], data[1],
                              errors[0][0], errors[0][1], errors[1][0], errors[1][1],
                              style, **kwargs)
            self._objects.append( obj )
            self._sources.append( obj.source )
        elif len(data) == 3: #2d graph
            self.check_excess_2d_()
            self._objects.append( Graph2D(self._figure, data[0], data[1], data[2],
                                          errors[0][0], errors[0][1], errors[1][0], errors[1][1],
                                          style, **kwargs) )
        else:
            raise error_handler('value', 'only 1D and 2D graphs are currently supported.')

    @fig_kwargs_manager
    def histogram(self, data, style, *args, **kwargs):
        if len(data) == 2: #1d histogram
            self._objects.append( Histogram1D(self._figure, data, style, **kwargs) )
        elif len(data) == 3:
            self.check_excess_2d_()
            self._objects.append( Histogram2D(self._figure, data, style, **kwargs) )
        else:
            raise error_handler('value', 'only 1D and 2D histograms are currently supported.')

    @fig_kwargs_manager
    def label(self, label, x, y, *args, **kwargs):
        self._figure.add_layout( Label(x=x, y=y, text=label, *args, **kwargs) )

    @fig_kwargs_manager
    def line(self, x, y, *args, **kwargs):
        self._figure.line(x, y, *args, **kwargs)

    def _set_fig_kwargs(self):
        if self._fig_kwargs is not None:
            orig_kw, axis_kw, title_kw, legend_kw, toolbar_kw = (dict() for _ in range(5))
            for k,w in self._fig_kwargs.items():
                if 'x.' in k or 'y.' in k:
                    k2 = self._extract_axis_key(k)[0] if k.count('.') == 2 else k
                    if k2 not in self._axis_kw:
                        print('Please choose one of the following: ', self._axis_kw)
                        error_handler('value',
                                      k2[2:] + ' is not a valid option for an axis.')
                    else:
                        axis_kw.update({k:w})
                elif 't.' in k:
                    if k not in self._title_kw:
                        print('Please choose one of the following: ', self._title_kw)
                        error_handler('value',
                                      k[2:] + ' is not a valid option for a title.')
                    else:
                        title_kw.update({k:w})
                elif 'l.' in k:
                    if k not in self._legend_kw:
                        print('Please choose one of the following: ', self._legend_kw)
                        error_handler('value',
                                      k[2:] + ' is not a valid option for a legend.')
                    else:
                        legend_kw.update({k:w})
                elif 'toolbar.' in k:
                    if k not in self._toolbar_kw:
                        print('Please choose one of the following: ', self._toolbar_kw)
                        error_handler('value',
                                      k[2:] + ' is not a valid option for a toolbar.')
                    else:
                        toolbar_kw.update({k:w})
                else:
                    orig_kw.update({k:w})

            self._set_fig_original_kw(orig_kw)
            self._set_fig_axis_kw(axis_kw)
            self._set_fig_title_kw(title_kw)
            self._set_fig_legend_kw(legend_kw)
            self._set_fig_toolbar_kw(toolbar_kw)
                
    def _set_fig_axis_kw(self, fig_kwargs):
        """
        Set axis properties. These can be defined as follows by the user:
        - x.<property name> for changing all x axis in a figure 
        - x.<id>.<property name> for changing x axis number <id>
        The same is valid for the y axis by rpleacing 'x.' by 'y.'.
        """
        for k,w in fig_kwargs.items():
            if k.count('.') == 1: #apply value to all axis
                axis_idx = None 
            elif k.count('.') == 2: #apply value to a specific axis
                #overwrite key `k` and set axis index
                k, axis_idx = self._extract_axis_key(k)
            else:
                raise error_handler('value', 'You cannot have more than two dots in the axis porperty.')

            if 'x.' in k:
                if not axis_idx:
                    setattr(self._figure.xaxis, re.sub('x\.', '', k), w)
                else:
                    setattr(self._figure.xaxis[axis_idx], re.sub('x\.', '', k), w)
            elif 'y.' in k:
                if not axis_idx:
                    setattr(self._figure.yaxis, re.sub('y\.', '', k), w)
                else:
                    setattr(self._figure.yaxis[axis_idx], re.sub('y\.', '', k), w)
            else:
                raise error_handler('runtime', 'The code should not have gotten to this point. Check `Figure._axis_kw`.')

    def _set_fig_legend_kw(self, fig_kwargs):
        for k,w in fig_kwargs.items():
            if 'l.' in k:
                setattr(self._figure.legend, re.sub('l\.', '', k), w)
            else:
                raise error_handler('runtime', 'The code should not have gotten to this point. Check `Figure._legend_kw`.')
    def _set_fig_original_kw(self, fig_kwargs):
        for k,w in fig_kwargs.items():
            setattr(self._figure, k, w)

    def _set_fig_title_kw(self, fig_kwargs):
        for k,w in fig_kwargs.items():
            if 't.' in k:
                if '\n' in w:
                    lines = w.split('\n')
                    title_kw = {re.sub('t\.', '', _k): _v for (_k,_v) in fig_kwargs.items() if 't.' in _k and _k != 't.text'}
                    for l in lines[::-1]:
                        if l == lines[0]:
                            self._figure.add_layout(Title(text=l, **title_kw), 'above')
                        else:
                            self._figure.add_layout(Title(text=l, text_font_style="italic", **title_kw), 'above')
                else:
                    setattr(self._figure.title, re.sub('t\.', '', k), w)
            else:
                raise error_handler('runtime', 'The code should not have gotten to this point. Check `Figure._title_kw`.')

    def _set_fig_toolbar_kw(self, fig_kwargs):
        for k,w in fig_kwargs.items():
            if 'toolbar.' in k:
                setattr(self._figure.toolbar, re.sub('toolbar\.', '', k), w)
            else:
                raise error_handler('runtime', 'The code should not have gotten to this point. Check `Figure._toolbar_kw`.')
            
    def _set_fig_original_kw(self, fig_kwargs):
        for k,w in fig_kwargs.items():
            setattr(self._figure, k, w)
