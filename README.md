[![Anaconda-Server Badge](https://anaconda.org/bfontana/bokehplot/badges/version.svg)](https://anaconda.org/bfontana/bokehplot)

![BokehPlot](https://bitbucket.org/bfontana/bokehplot/raw/d0607fabc6a6bed62cd5ad3506d7efa75332560e/BokehPlot.png "a title")

**BokehPlot**: the straightforward Bokeh wrapper
=========================

**Goals**

- *1)* Simplify commonly used Bokeh plotting tools
- *2)* Avoid looking at the same online examples over and over again due to unfrequent usage

---------------------------------------------

Structure
-------------------------

The code is structured in a object-oriented way: 

**Stack** → **Frame** → **Figure** → **Objects**

```BokehPlot.Figure()``` objects are organized in a ```BokehPlot.Frame``` which represents what can be seen in the browser or exported to a picture. Within a frame, figures are indexed either linearly or using a square grid. Frames are organized in a ```BokehPlot.Stack()``` object. Multiple stacks are not supported; simply create a new ```BokehPlot()``` object in case you need multiple stacks. Each ```Figure()``` may contain multiple objects, such as a ```Histogram1D```, ```Graph2D```, etc. 

Operations are defined directly on the figures: whenever one is changed via some **BokehPlot** method (```BokehPlot.histogram()``` that calls ```BokehPLot.Histogram1D()```, for instance), the modifications are immediately available; in other words, the objects contained in the frames are always set to be ```bokeh.plotting.figure()```.


----------------------------------------------

**Notes on textures**

Any histogram or bar graph should in principle be customizable with the following texture parameters:

*Texture type*:

    - . dot
    - o ring
    - + cross
    - " horizontal-dash
    - : vertical-dash
    - - horizontal-line
    - | vertical-line
    - / right-diagonal-line
    - \ left-diagonal-line
    - X diagonal-cross
    - `  right-diagonal-dash
    - , left-diagonal-dash
    - v horizontal-wave
    - > vertical-wave
    - * criss-cross

*Texture weight*: how "thick" the textures are

*Texture color*

These parameters can be passed as usual using keyword arguments (```hatch_pattern```, ```hatch_weight``` and ```hatch_color```), but to make things easier for the user, the ```style``` parameter can be used, and specified as follows:

```style = <type>%<weight>%color```

This is still an undocumented ```bokeh``` feature. More can be read [here](https://github.com/bokeh/bokeh/pull/8859).

*Note*: Colors in ```bokeh``` can be specified as shown [here](https://docs.bokeh.org/en/latest/docs/user_guide/styling.html#specifying-colors)

**Export ```BokehPlot.Figure()``` as a single picture**

Please use either ```save_fig()``` or ```save_figs()```, depending on whether you want to save all figures in a frame or not.

You can either specify the frame (and figure index when using ```save_fig()```), or else **BokehPlot** will try to guess which frame you meant, just like in any other operation within this package.

If you do not specify the picture's ```filename```, **BokehPlot** will use the corresponding frame's ```filename```, defined when a frame is created, and will add a number to avoid name duplication. If you do not specify the ```path```, the frame's path will be also used instead.

The only mode currently supported is ```mode='png'```.


Interaction
---------------------

One of the major benefits of ```bokeh``` is the ability to add interactive elements to the plots, which are dubbed [widgets](https://docs.bokeh.org/en/latest/docs/user_guide/interaction/widgets.html). Two approaches are available to add them, which are also available in ```bokehplot```:

- **Python callbacks**

HTML pages in the browser cannot run ```python``` code. Therefore, when using Python callbacks (see ```tests/dropdown.py``` for an example), one first has to run a bokeh server as follows:

```bash
bokeh serve &
python tests/dropdown.py
```

The user should also use the function ```display_frame()``` and ```display_frames()``` instead of the previously mentioned ```save_frames()``` and ```save_figs()```.

- **Custom javascript callbacks**



SSH Forwarding
---------------------

It allows ```save_frame(show=True)``` to work while on a remote server. There has to be a connection between a local browser and the server where the code is being run.
To simplify the procedure, it is recommended to use the same port number for both the forwarding  and the browser display.


```bash
ssh -L <port1>:localhost:<port2> <server name>
bokeh serve --allow-websocket-origin=localhost:<port1> --port=<port2> <optional python script> --args -csv some_file.csv
```
After this, you should be able to visualize the output of any ```Python``` script you run on your server by going to a local browser of yours and typing ```localhost:<port1>``` in the search bar. Actually, ```save_frame(show=True)``` opens a new tab automatically.

Requirements
-----------------

- numpy
- scipy
- colorcet

**```export_png()```**

```bash
conda config --prepend pkgs_dirs /tmp/<username>/conda/pkgs
conda create --prefix /tmp/<username>/test-env
conda activate /tmp/<username>/test-env
conda install bokeh
conda install selenium
conda install -c conda-forge firefox geckodriver
```