import os
import numpy as np
import bokehplot as bkp

arr_x = np.array([x*0.005 for x in range(0, 200)])
arr_y = arr_x
b = bkp.BokehPlot( os.path.join(os.environ['HOME'], 'graph_int.html'), nfigs=1, nwidgets=1)
b.graph(idx=0, data=[arr_x,arr_y], style='circle', color='orange', line=False)

callback = """var data = source0_0.data; 
var f = cb_obj.value;
var x = data['x'];
var y = data['y'];
for (var i = 0; i < x.length; i++) {
y[i] = Math.pow(x[i], f);
}
source.change.emit();"""

b.widget('slider', callback=callback, link_to_figures=[0], link_to_objects=[[0]],
         start=0.1, end=4, value=1, step=.1, title="power")
b.save_frame(show=False)
#b.save_figs(path='.', mode='png')
