import os
import numpy as np
import bokehplot as bkp

from bokeh.models import Range1d

# produce data
npoints = 15
shift, shift2, margin = 5., 2., .1
x = np.arange(0,npoints)
y1 = np.linspace(shift, shift+1, npoints)
y2 = np.linspace(shift2, shift2+1, npoints)[::-1]
exd, exu = [np.zeros(npoints) for _ in range(2)]
eyd, eyu = [0.01*np.ones(npoints) for _ in range(2)]

# create html page
file_name = os.path.basename(__file__)[:-2] + 'html'
b = bkp.BokehPlot( os.path.join(os.environ['HOME'], file_name), nfigs=1)

# add axis to figure number 0
new_axis_name = 'right_axis'
b.add_axis( idx=0, name=new_axis_name, location='right',
            atype='y', arange=Range1d(shift2-margin,shift2+1.+margin) )

# define properties
colors = ('brown', 'blue')
fig_kwargs = {'y_range': Range1d(shift-margin,shift+1.+margin,),
              'y.0.axis_label': 'Y1',
              'y.1.axis_label': 'Y2',
              'y.0.axis_label_text_color': colors[0],
              'y.1.axis_label_text_color': colors[1],
              'y.0.major_label_text_color': colors[0],
              'y.1.major_label_text_color': colors[1],
              'y.0.axis_line_color': colors[0],
              'y.1.axis_line_color': colors[1],
              'y.0.major_tick_line_color': colors[0],
              'y.1.major_tick_line_color': colors[1] }

# draw graphs and associate the data with the axis
# control whisker properties (defaults to line properties)
b.graph(idx=0, data=[x,y1],
        errors=[[exd,exu],[eyd,eyu]],
        style='circle', color=colors[0], line=True,
        fig_kwargs=fig_kwargs)
b.graph(idx=0, data=[x,y2],
        errors=[[exd,exu],[eyd,eyu]],
        style='square', color=colors[1], line=True,
        whisker_length=6., whisker_width=3.,
        y_range_name=new_axis_name, fig_kwargs=fig_kwargs)

# save the frame and display at once
b.save_frame(show=False)

# save the figures as PNG
# b.save_figs(path='.', mode='png')
