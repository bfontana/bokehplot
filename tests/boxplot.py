import numpy as np
import bokehplot as bkp

size = 1000
arr = np.random.normal(0,2,size)
cats1 = np.random.random_integers(low=5,high=15,size=size)
cats2 = np.concatenate( (np.ones(500), np.zeros(500)) )
cats3 = 100*list('abcdefghij')

b = bkp.BokehPlot('boxplot.html', nfigs=4)
b.boxplot(data=[[arr,cats1],[arr,cats2]], idx=[0,1], outliers_factor=.5, outliers_color='purple', color='blue')
b.boxplot(data=[cats3,arr], idx=3, style='horizontal', outliers_factor=1.5, outliers_color='orange', color='brown', line_color='red')
b.boxplot(data=[cats2,arr], idx=2, style='horizontal', outliers_factor=1.5, outliers_color='orange', color='brown', line_color='red')
b.save_frame(nrows=2,ncols=2)
