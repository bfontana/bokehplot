import os
import numpy as np
import bokehplot as bkp


size = 1000
arr = np.random.normal(0,2,size)
cats = np.random.randint(5,16,size=size)

b = bkp.BokehPlot( os.path.join(os.environ['HOME'],'dropdown.html'), nfigs=1, nwidgets=2)
b.widget('dropdown', label='A', menu=['a', 'b', 'c'], action=None)
b.widget('dropdown', label='B', menu=['a', 'b', 'c'], action=None)
b.boxplot(data=[arr,cats], outliers_factor=.5, outliers_color='purple', color='blue')
b.save_frame(show=True)
