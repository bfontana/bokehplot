import os, subprocess
import numpy as np
import bokehplot as bkp

npoints = 5
gpu_x = np.arange(1, npoints+1)
gpu_y = np.array([1.,2.,3.,5.,4.])
cpu_x = gpu_x
cpu_y = gpu_y*2.
ratio_x = cpu_x
ratio_y = cpu_y / gpu_y
b = bkp.BokehPlot('speedup.html', nfigs=1)

"""
These quantities allow to override the labels in Bokeh, and can be conveniently passed to fig_kwargs in BokehPlot
If a Bokeh variable is not directly available in fig_kwargs, one can always set them in the figure by calling
BokehPlot.get_figure(...)
"""
options = {'x.ticker': [1, 2, 3, 4, 5], 
           'x.major_label_overrides': {1: '0', 2: '50', 3: '100', 4: '140', 5: '200'} }

b.graph(idx=0, data=[gpu_x,cpu_y], style='circle', color='blue', line=True, legend_label="GPU", fig_kwargs=options)
b.graph(idx=0, data=[gpu_x,gpu_y], style='square', color='red', line=True, legend_label="CPU")
b.graph(idx=0, data=[ratio_x,ratio_y], style='triangle', color='green', line=True, legend_label="speed-up")

b.save_figs(path='.', mode='png')
