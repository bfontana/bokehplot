"""
This example shows how one can export a group of figures as PNG files.
When `filename` of `save_fig()` or `save_figs()` is not set, the name of the output is taken from the corresponding frame.
"""

import numpy as np
import bokehplot as bplot

x = np.random.normal(0, 4*np.pi, 100)
y = np.sin(x)

b = bplot.BokehPlot(filenames='myframe.html', nframes=1, nfigs=2)
b.histogram(idx=0, data=np.histogram(x), color='grey', style='.%0.4%purple')
b.graph(idx=1, data=[x,y], style='circle', line=False)
b.save_figs(filename=None, path='tests/figs/', iframe=None, mode='png')
