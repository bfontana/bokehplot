import numpy as np
import bokehplot as bkp
from bokeh.io import export_png

arr = np.random.normal(0,50,(1000,2))
b = bkp.BokehPlot('test.html', nfigs=3)
b.histogram(idx=0, data=np.histogram(arr[:,0]), color='orange', style='v%0.8%red')
b.graph(idx=1, data=[np.arange(1,11),np.arange(1,20,2)], style='vbar%"%2%orange', line=True)
b.histogram(idx=2, data=np.histogram2d(arr[:,0],arr[:,1],bins=50), style='quad%Viridis')
b.save_frame(show=False)
