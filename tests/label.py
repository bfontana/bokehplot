import numpy as np
import bokehplot as bkp
from bokeh.io import export_png

arr = np.random.normal(0,5,(500))
b = bkp.BokehPlot('label_test.html', nfigs=1)
b.histogram(idx=0, data=np.histogram(arr), color='purple', style='v%0.8%orange')

pm = u'\u00B1'
label = 'result ' + pm + ' uncertainty'
additional_kwargs = {'text_font_size': '9pt', 'x_units': 'screen', 'y_units': 'screen'}
b.label(label, idx=0, iframe=0, x=10, y=240, **additional_kwargs)
b.save_fig(path='figs/', mode='png')
